# Garage slack bot

## Purpose

This is a learning project. The goal is to create a slack bot as an interface to shopware. You will be able to search and list products from a Shopware shop inside slack, register with the shop and buy products.

## Development

Since the main goal of the app is learning more about how Shopware works on a technical level, this project is developed in iterations.

Each iteration corresponds to one or more meetings/lessons, aimed to teach a certain aspect of software development. For a list of iterations see below.

If an iteration is not yet complete, placeholder text is used. As soon as the iteration is completed, it is replaced by a description.

# Iterations

## 000 - Set your path

### Software Requirements

- ein Shop ist angebunden
- woher kommen die Daten vom Kunden (Name etc.), ob aus Slack oder ein Schritt bei der Bestellung
- wer im Slack ist, darf einkaufen
- Bestellungen durchführen
- Produktsuche
- Produktlisting
- Sale-Kategorie anzeigen
- Infos zum Produkt anfragen
- Bewertungen abfragen und abgeben
- Lieblingsprodukt per Slack teilen
- vordefinierte Liste einkaufen (z. B. Alles für Flammkuchen)
- Niklas wünscht sich Warenkorb (!), anzeigen und bearbeiten

### List of commands

| Befehl                                        | Antwort                                                                               |
|-----------------------------------------------|---------------------------------------------------------------------------------------|
| @garagebot                                    | Allgemeine Infos zu dem Bot und  den Befehlen                                         |
| @garagbot &lt;categories&gt;                  | Alle Shop-Kategorien werden im  Slack angezeigt                                       |
| @garagebot &lt;tags&gt;                       | Alle im Shop vergebenen Tags  anzeigen.                                               |
| @garagebot &lt;products&gt;                   | Alle Produkte werden angezeigt.                                                       |
| @garagebot &lt;tags&gt; &lt;list of tags&gt;  | zeigt direkt alle Produkte unter  diesem oder mehreren Tags                           |
| @garagebot &lt;search&gt; &lt;search term&gt; | Der Shop wird nach einem  bestimmten Begriff durchsucht und Treffer werden angezeigt. |

## 001 - Choose your weapons

We use Go for building the bot.
Go-package we will use: [slacker](https://github.com/shomali11/slacker) and later Viper.

## 002 - Hello World

> Before we start, we need an introdution to the things we choose in step 1   
> Additionally, we need a working Shopware instance

## 003 - Toe in the water

> Before connecting Shopware and Slack, we need a proof of concept.   
> Create a simple but working slack bot. Use insomnia to talk to the Shopware API.